/**
 * Created by artemkopytko on 05.09.17.
 */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');
// const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');
const glob = require('glob-all');


module.exports = {
    entry: {
        'home': './src/app.js',
        '404': './src/404/404.js',
        'contact': './src/contact/contact.js',
        'services': './src/services/services.js',
        'about_us': './src/about_us/about_us.js'
    },
    output: {
        path: __dirname,
        filename: 'pages/[name]/js/[name]bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [ 'css-loader?url=false', 'sass-loader?url=false' ],
                    publicPath: '/dist'
            })
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use:
                    [
                        'file-loader?name=pages/src/images/[name].[ext]',
                        'image-webpack-loader'
                    ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Home',
            minify: {
                collapseWhitespace: false
            },
            filename: 'pages/home/index.html',
            hash: true,
            chunks: ['home'],
            template: './src/index.html' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new HtmlWebpackPlugin({
            title: '404',
            minify: {
                collapseWhitespace: false
            },
            filename: 'pages/404/index.html',
            hash: true,
            chunks: ['404'],
            template: './src/404/index.html' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new HtmlWebpackPlugin({
            title: 'about_us',
            minify: {
                collapseWhitespace: false
            },
            filename: 'pages/about_us/index.html',
            hash: true,
            chunks: ['about_us'],
            template: './src/about_us/index.html' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new HtmlWebpackPlugin({
            title: 'contact',
            minify: {
                collapseWhitespace: false
            },
            filename: 'pages/contact/index.html',
            hash: true,
            chunks: ['contact'],
            template: './src/contact/index.html' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new HtmlWebpackPlugin({
            title: 'services',
            minify: {
                collapseWhitespace: false
            },
            filename: 'pages/services/index.html',
            hash: true,
            chunks: ['services'],
            template: './src/services/index.html' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new ExtractTextPlugin("/pages/[name]/css/[name].css"),
        new webpack.ProvidePlugin({ // inject ES5 modules as global vars
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether'
        }),
        new PurifyCSSPlugin({
            // Give paths to parse for rules. These should be absolute!
            paths: glob.sync([
                path.join(__dirname, 'src/*.html'),
                path.join(__dirname, 'src/404/*.html'),
                path.join(__dirname, 'src/about_us/*.html'),
                path.join(__dirname, 'src/services/*.html'),
                path.join(__dirname, 'src/contact/*.html')
                ]),
            minimize: true
        })
    ]
}